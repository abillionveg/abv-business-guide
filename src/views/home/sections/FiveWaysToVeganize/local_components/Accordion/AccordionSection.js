import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

class AccordionSection extends Component {
  static propTypes = {
    children: PropTypes.instanceOf(Object).isRequired,
    isOpen: PropTypes.bool.isRequired,
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.container = React.createRef();
  }

  componentDidUpdate(prevProps) {
    if (prevProps !== this.props && this.props.isOpen) {
      window.scrollTo(0, this.container.current.offsetTop);
    }
  }

  handleOnClick(label) {
    this.props.onClick(label);
  }

  render() {
    const { isOpen, label, isFirstSection } = this.props;

    return (
      <Container ref={this.container}>
        <LabelDiv onClick={() => this.handleOnClick(label)} isOpen={isOpen}>
          <H2 isFirstSection={isFirstSection}>
            {label} {!isOpen && <SectionIndicator>+</SectionIndicator>}
            {isOpen && <SectionIndicator>-</SectionIndicator>}
          </H2>
        </LabelDiv>
        {isOpen && <BulletPointsDiv>{this.props.children}</BulletPointsDiv>}
      </Container>
    );
  }
}

export default AccordionSection;

const Container = styled.div``;

const H2 = styled.h2`
  margin-block-start: ${props => (props.isFirstSection ? 0 : "0.83em")};
  font-size: 16px;
  @media (min-width: 600px) {
    font-size: 20px;
  }
  @media (min-width: 900px) {
    font-size: 24px;
  }
  @media (min-width: 1200px) {
    font-size: 32px;
  }
  &:hover {
    text-shadow: 0px 0px 2px white;
  }
`;

const LabelDiv = styled.div`
  border-bottom: ${props => (props.isOpen ? "none" : "3px solid white")};
  font-weight: 600px;
  cursor: pointer;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const SectionIndicator = styled.span`
  margin-left: 0.5em;
  @media (min-width: 600px) {
    margin-left: 1em;
  }
`;

const BulletPointsDiv = styled.div`
  font-size: 20px;
  font-weight: 500;
  padding: 1em 0;
  @media (min-width: 600px) {
    padding: 1em 0 1em 3em;
    font-size: 22px;
  }
`;
