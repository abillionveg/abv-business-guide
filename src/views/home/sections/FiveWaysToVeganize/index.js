import React, { Component } from "react";
import PropTypes from "prop-types";

import FiveWaysToVeganizeContainer from "./FiveWaysToVeganizeContainer.js";

class FiveWaysToVeganizeSection extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired
  };

  render() {
    const { copy } = this.props;
    return <FiveWaysToVeganizeContainer copy={copy} />;
  }
}

export default FiveWaysToVeganizeSection;
