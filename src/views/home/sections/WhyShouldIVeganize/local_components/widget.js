import React from "react";
import styled from "styled-components";

const Widget = ({
  content,
  backgroundColor,
  bgColorOnHover,
  onClick,
  moreStyles
}) => {
  return (
    <WrapperContainer {...moreStyles}>
      <Container
        backgroundColor={backgroundColor}
        bgColorOnHover={bgColorOnHover}
        onClick={onClick}
      >
        <ContentDivMobile>
          <H2>
            {content} <br />>>
          </H2>
        </ContentDivMobile>

        <ContentDivDesktop>
          <H2>
            {content} <br /> >>
          </H2>
        </ContentDivDesktop>
      </Container>
    </WrapperContainer>
  );
};

export default Widget;

const WrapperContainer = styled.div`
  width: 100%;
  order: ${props => props.order};
  @media (min-width: 600px) {
    width: 50%;
    order: 1;
    padding: 0 0.5em;
  }
  @media (min-width: 900px) {
    padding: 0 1em;
  }
`;
const Container = styled.div`
  background: ${props => props.backgroundColor};
  border-radius: 20px;
  flex: 1;
  margin-bottom: 1em;
  min-height: 140px;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  min-height: 150px;
  @media (min-width: 600px) {
    border-radius: 20px;
    width: 100%;
    height: 0;
    padding-bottom: 100%;
    position: relative;
    display: block;
    min-height: initial;
    margin-bottom: 2em;
  }
  @media (min-width: 1200px) {
    margin-bottom: 3em;
  }
  &:hover {
    background: ${props => props.bgColorOnHover}
    border-radius: 20px;
    transition: background 0.3s ease;
  }
`;

const ContentDivDesktop = styled.div`
  display: none;
  @media (min-width: 600px) {
    position: absolute;
    width: 100%;
    height: 100%;
    align-items: center;
    display: flex;
    justify-content: center;
  }
`;
const ContentDivMobile = styled.div`
  @media (min-width: 600px) {
    display: none;
  }
`;

const H2 = styled.h2`
  color: white;
  padding: 1em 1em;
  font-size: 18px;
  text-align: center;
  @media (min-width: 600px) {
    padding: 1em;
    font-size: 12px;
  }
  @media (min-width: 700px) {
    padding: 1em;
    font-size: 16px;
  }
  @media (min-width: 900px) {
    padding: 1em 1em;
    font-size: 24px;
  }
  @media (min-width: 1200px) {
    font-size: 32px;
  }
`;
