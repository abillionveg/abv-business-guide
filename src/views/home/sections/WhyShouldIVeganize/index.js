import React, { Component } from "react";
import PropTypes from "prop-types";

import WhyShouldIVeganizeContainer from "./WhyShouldIVeganizeContainer.js";

class WhyShouldIVeganizeSection extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired,
    routes: PropTypes.object.isRequired
  };

  render() {
    const { copy, routes } = this.props;
    return <WhyShouldIVeganizeContainer copy={copy} routes={routes} />;
  }
}

export default WhyShouldIVeganizeSection;
