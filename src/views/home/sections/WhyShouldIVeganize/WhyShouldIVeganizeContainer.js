import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import styled from "styled-components";

import Modal from "../../../../globalComponents/modal.js";
import Widget from "./local_components/widget.js";

import theme from "../../../../globalStyles.js";

import BackgroundPinkMobilePng from "../../../../assets/background-pink-mobile.png";
import BackgroundPinkPng from "../../../../assets/background-pink.png";
import Waiter1Png from "../../../../assets/waiter-1.png";
import Pan1Png from "../../../../assets/pan-1.png";
import Pan2Png from "../../../../assets/pan-2.png";

class WhyShouldIVeganizeContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalIsVisible: false,
      activeWidget: ""
    };
  }

  static propTypes = {
    copy: PropTypes.object.isRequired,
    routes: PropTypes.object.isRequired
  };

  handleWidgetClick(widgetName) {
    this.setState({
      modalIsVisible: true,
      activeWidget: widgetName
    });
  }

  handleModalClose(e) {
    this.setState({
      modalIsVisible: false,
      activeWidget: ""
    });
  }

  renderHeader() {
    const { copy } = this.props;
    return (
      <HeaderDiv>
        <ImageDivLeft>
          <Img src={Waiter1Png} alt="waiter-1" />
        </ImageDivLeft>
        <H1>{copy.whyShouldIVeganize}</H1>
        <ImageDivRight>
          <Img src={Waiter1Png} alt="waiter-1" />
        </ImageDivRight>
      </HeaderDiv>
    );
  }

  renderWidgets() {
    const { copy } = this.props;

    return (
      <WidgetContainer>
        <RowTop>
          <Widget
            content={copy.aBoomingVeganMarket.title}
            backgroundColor={theme.colorPinkWithOpacity}
            bgColorOnHover={theme.colorDarkPink}
            onClick={() =>
              this.handleWidgetClick(copy.aBoomingVeganMarket.title)
            }
          />
          <Widget
            content={copy.morePeopleGoingVegan.title}
            backgroundColor={theme.colorPurpleWithOpacity}
            bgColorOnHover={theme.colorDarkPurple}
            onClick={() =>
              this.handleWidgetClick(copy.morePeopleGoingVegan.title)
            }
          />
        </RowTop>
        <RowBottom>
          <Widget
            content={copy.reduceEnvImpact.title}
            backgroundColor={theme.colorPurpleWithOpacity}
            bgColorOnHover={theme.colorDarkPurple}
            onClick={() => this.handleWidgetClick(copy.reduceEnvImpact.title)}
            moreStyles={{ order: 2 }}
          />

          <Widget
            content={copy.buildCommunity.title}
            backgroundColor={theme.colorPinkWithOpacity}
            bgColorOnHover={theme.colorDarkPink}
            onClick={() => this.handleWidgetClick(copy.buildCommunity.title)}
          />
        </RowBottom>
      </WidgetContainer>
    );
  }

  renderABoomingVeganMarketContent() {
    const { copy } = this.props;
    return (
      <React.Fragment>
        {copy.aBoomingVeganMarket.body.map(para => (
          <p key={para}>{para}</p>
        ))}
      </React.Fragment>
    );
  }

  renderMorePeopleGoingVeganContent() {
    const { copy } = this.props;
    return (
      <React.Fragment>
        {copy.morePeopleGoingVegan.body.map(para => (
          <p key={para}>
            <BulletPoint color="purple" />
            {para}
          </p>
        ))}
      </React.Fragment>
    );
  }

  renderReduceEnvImpactContent() {
    const { copy } = this.props;
    return (
      <React.Fragment>
        {copy.reduceEnvImpact.body.map(para => (
          <p key={para}>
            <BulletPoint color="pink" />
            {para}
          </p>
        ))}
      </React.Fragment>
    );
  }

  renderBuildCommunityContent() {
    const { copy } = this.props;
    return (
      <React.Fragment>
        {copy.buildCommunity.body.map(para => (
          <p key={para}>{para}</p>
        ))}
      </React.Fragment>
    );
  }

  renderModalContent() {
    const { activeWidget } = this.state;
    const { copy } = this.props;
    let content;
    switch (activeWidget) {
      case copy.aBoomingVeganMarket.title:
        content = this.renderABoomingVeganMarketContent();
        break;
      case copy.morePeopleGoingVegan.title:
        content = this.renderMorePeopleGoingVeganContent();
        break;
      case copy.reduceEnvImpact.title:
        content = this.renderReduceEnvImpactContent();
        break;
      case copy.buildCommunity.title:
        content = this.renderBuildCommunityContent();
        break;
      default:
        return;
    }
    return content;
  }

  getMoreStyles() {
    const { activeWidget } = this.state;
    const { copy } = this.props;
    let style;
    switch (activeWidget) {
      case copy.aBoomingVeganMarket.title:
        style = {
          border: `5px solid ${theme.colorPink}`,
          color: theme.colorPink
        };
        break;
      case copy.buildCommunity.title:
        style = {
          border: `5px solid ${theme.colorPurple}`,
          color: theme.colorPurple
        };
        break;
      case copy.reduceEnvImpact.title:
        style = {
          border: `5px solid ${theme.colorPink}`,
          color: theme.colorPink
        };
        break;
      case copy.morePeopleGoingVegan.title:
        style = {
          border: `5px solid ${theme.colorPurple}`,
          color: theme.colorPurple
        };
        break;
      default:
        return;
    }
    return style;
  }

  renderQuizBanner() {
    const { copy } = this.props;
    return (
      <QuizContent>
        <ImgDiv>
          <Img src={Pan1Png} alt="pan-1" />
        </ImgDiv>
        <div>
          <QuizTitle> {copy.howVeganFriendlyAreYou}</QuizTitle>
          <QuizSubtitle>{`>> ${copy.takeOurQuiz} <<`}</QuizSubtitle>
        </div>
        <ImgDiv>
          <Img src={Pan2Png} alt="pan-1" />
        </ImgDiv>
      </QuizContent>
    );
  }

  render() {
    const { modalIsVisible, activeWidget } = this.state;
    const { routes } = this.props;
    return (
      <BgWrapper>
        <Container>
          <ContentDiv>
            {this.renderHeader()}
            {this.renderWidgets()}
          </ContentDiv>
          <Link to={routes.quiz} target="_blank">
            <QuizBannerDiv>{this.renderQuizBanner()}</QuizBannerDiv>
          </Link>
        </Container>
        <Modal
          isVisible={modalIsVisible}
          handleClose={e => this.handleModalClose(e)}
          // breadcrumbItems={[copy.whyShouldIVeganize, activeWidget]}
          moreStyles={this.getMoreStyles()}
          title={activeWidget}
        >
          <ModalContentDiv>{this.renderModalContent()}</ModalContentDiv>
        </Modal>
      </BgWrapper>
    );
  }
}

export default WhyShouldIVeganizeContainer;

const ModalContentDiv = styled.div`
  margin-top: 160px;
  @media (min-width: 600px) {
    margin-top: 90px;
  }
`;

const BgWrapper = styled.div`
  background-image: url(${BackgroundPinkMobilePng});
  width: 100%;
  background-size: cover;
  background-position: top center;
  padding-bottom: 2em;
  @media (min-width: 600px) {
    background-image: url(${BackgroundPinkPng});
    background-repeat: no-repeat;
  }
  @media (min-width: 1200px) {
    min-height: 100vh;
  }
`;

const Container = styled.div`
  max-width: ${props => props.theme.maxContentWidth};
  margin: 0 auto;
  a {
    text-decoration: none !important;
    color: inherit;
  }
`;

const ContentDiv = styled.div`
  padding: 1.5em;
  @media (min-width: 600px) {
    width: 80%;
    margin: 0 auto;
    display: flex;
    padding: 4em 3em;
  }
`;

const HeaderDiv = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  color: ${props => props.theme.colorPurple};
  margin-bottom: 2em;
  @media (min-width: 600px) {
    flex-direction: column;
    align-items: flex-start;
    justify-content: flex-start
    flex: 1;
  }
`;

const H1 = styled.h1`
  padding-left: 0.2em;
  padding-right: 0.2em;
  overflow: hidden;
  margin: 0;
  text-align: center;
  font-size: 20px;
  @media (min-width: 600px) {
    padding-left: 0;
    padding-right: 1em;

    font-size: 20px;
    text-align: left;
    margin-bottom: 1em;
  }
  @media (min-width: 800px) {
    font-size: 24px;
  }
  @media (min-width: 900px) {
    font-size: 28px;
  }
  @media (min-width: 1200px) {
    font-size: 36px;
  }
`;

const ImageDivLeft = styled.div`
  width: 25%;
  @media (min-width: 600px) {
    order: 2;
    width: 60%;
  }
`;

const ImageDivRight = styled.div`
  width: 25%;
  -moz-transform: scale(-1, 1);
  -webkit-transform: scale(-1, 1);
  -o-transform: scale(-1, 1);
  -ms-transform: scale(-1, 1);
  transform: scale(-1, 1);
  @media (min-width: 600px) {
    display: none;
  }
`;

const Img = styled.img`
  width: 100%;
  height: auto;
`;

const WidgetContainer = styled.div`
  @media (min-width: 600px) {
    flex: 2;
    display: flex;
    flex-direction: column;
  }
`;

const RowTop = styled.div`
  display: flex;
  flex-direction: column;
  @media (min-width: 600px) {
    padding: 0 1em;
    flex-direction: row;
  }
  @media (min-width: 1200px) {
    padding: 0 2em;
  }
`;
const RowBottom = styled.div`
  display: flex;
  flex-direction: column;
  @media (min-width: 600px) {
    padding: 0 1em;
    flex-direction: row;
  }

  @media (min-width: 1200px) {
    padding: 0 2em;
  }
`;

const BulletPoint = styled.span`
  display: inline-block;
  height: 10px;
  width: 10px;
  margin-right: 2em;
  background-color: ${props =>
    props.color === "purple" ? props.theme.colorPurple : props.theme.colorPink};
  transform: rotate(45deg);
`;

const QuizBannerDiv = styled.div`
  width: 75%;
  margin: 0 auto;
  border: 5px solid ${props => props.theme.colorGreen};
  padding: 0.5em 0.5em;
  background: white;
  @media (min-width: 600px) {
    display: block;
    padding: 1em;
    margin-bottom: 2em;
  }
  &:hover {
    box-shadow: 4px 0px 4px 4px ${props => props.theme.colorLightGreen};
    transition: box-shadow 0.5s ease;
  }
`;

const QuizContent = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  cursor: pointer;
`;

const QuizTitle = styled.p`
  color: ${props => props.theme.colorGreen};
  text-transform: uppercase;
  font-weight: 800;
  font-size: 10px;
  margin: 0;
  text-align: center;
  @media (min-width: 400px) {
    font-size: 14px;
  }
  @media (min-width: 600px) {
    font-size: 24px;
  }
  @media (min-width: 900px) {
    font-size: 34px;
  }
  @media (min-width: 1200px) {
    font-size: 44px;
  }
  @media (min-width: 1800px) {
    font-size: 50px;
  }
`;

const QuizSubtitle = styled(QuizTitle)`
  color: ${props => props.theme.colorPink};
`;

const ImgDiv = styled.div`
  width: 10%;
  height: auto;
`;
