import React, { Component } from "react";
import PropTypes from "prop-types";

import ConclusionContainer from "./ConclusionContainer.js";

class ConclusionSection extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired,
    externalLinks: PropTypes.object.isRequired
  };

  render() {
    const { copy, externalLinks } = this.props;
    return <ConclusionContainer copy={copy} externalLinks={externalLinks} />;
  }
}

export default ConclusionSection;
