import React, { Component } from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";

import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";

class Menu extends Component {
  static propTypes = {
    caseStudies: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired,
    onClick: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      caseStudies: this.props.caseStudies
    };
  }

  render() {
    const { caseStudies, selectedItem } = this.props;
    return (
      <React.Fragment>
        <DesktopMenuWrapper>
          {caseStudies.map((item, index) => (
            <DesktopMenuItemContent
              key={index}
              onChange={() => this.props.onChange(index)}
              onClick={() => this.props.onClick(index)}
              isSelected={index === selectedItem}
            >
              <ImgMenuNameDiv isSelected={index === selectedItem}>
                <ImgDiv>
                  <Img
                    src={
                      index === selectedItem ? item.logoWhite : item.logoGrey
                    }
                  />
                </ImgDiv>
                <MenuItemName>{item.company}</MenuItemName>
              </ImgMenuNameDiv>
            </DesktopMenuItemContent>
          ))}
        </DesktopMenuWrapper>
        <MobileMenuWrapper>
          <Carousel
            centerMode={true}
            showArrows={false}
            showIndicators={false}
            showThumbs={false}
            centerSlidePercentage={50}
            dynamicHeight={true}
            showStatus={false}
            onChange={(index, e) => this.props.onChange(index, e)}
            onClickItem={(index, e) => this.props.onClick(index, e)}
            selectedItem={selectedItem}
          >
            {caseStudies.map((item, index) => (
              <MenuItem key={index}>
                <ImgDiv>
                  <Img src={item.logoWhite} />
                </ImgDiv>

                <MenuItemName>{item.company}</MenuItemName>
              </MenuItem>
            ))}
          </Carousel>
        </MobileMenuWrapper>
      </React.Fragment>
    );
  }
}

export default Menu;

const MobileMenuWrapper = styled.div`
  height: 170px;
  div {
    height: 100%;
    div {
      height: 100% !important;
      ul {
        height: 100% !important;
      }
    }
  }
  li {
    :not(.selected) {
      opacity: 0.4;
    }
  }
  li {
    background: white !important;
  }
  @media (min-width: 600px) {
    display: none;
  }
`;

const MenuItem = styled.div`
  background: ${props => props.theme.colorGreen};
  border-radius: 20px;
  margin: 0 0.5em;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color: white;
  font-weight: 600;
`;

const ImgDiv = styled.div`
  width: 70px;
  height: 70px;
  max-height: 70px;
  display: flex;
  align-items: center;
  @media (min-width: 900px) {
    width: 90px
    height: 90px
    max-height: 90px !important;
  }
  @media (min-width: 1200px) {
    width: 100px
    height: 100px
    max-height: 100px !important;
  }
  @media (min-width: 1800px) {
    width: 120px
    height: 120px
    max-height: 120px !important;
  }
`;

const Img = styled.img`
  width: 100%;
`;

const MenuItemName = styled.p`
  margin-bottom: 0;
  margin-top: 0.5em
  font-weight: 700;
  @media(min-width: 600px){
    font-size: 12px

  }
  @media(min-width: 900px){
    font-size: 16px
  }
  @media(min-width: 1200px){
    font-size: 20px
  }
  @media(min-width: 1800px){
    font-size: 24px
  }
`;

const DesktopMenuWrapper = styled.div`
  display: none;
  @media (min-width: 600px) {
    display: flex;
    background: ${props => props.theme.colorLightestGreen};
    border-radius: 20px;
    height: 150px;
  }
  @media (min-width: 900px) {
    height: 250px;
  }
`;

const DesktopMenuItemContent = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin: 1.5em 0;
  border-right: 1px solid ${props => props.theme.colorGrey};
  :last-child {
    border-right: 0px;
  }
  cursor: pointer;

    ${props =>
      props.isSelected &&
      css`
        border: 1px solid ${props => props.theme.colorGreen};
        cursor: pointer;
        background: ${props => props.theme.colorGreen};
        height: 180px;
        border-radius: 20px;
        margin-top: -15px;
        margin-left: -2px;
        color: white;
        &:hover {
          background: ${props => props.theme.colorDarkerGreen};
          border: 1px solid ${props => props.theme.colorDarkerGreen};
          transition: background 0.2s ease;
          opacity: 1;
        }
        @media (min-width: 900px) {
          height: 300px;
          margin-top: -20px;
        }
      `};
  }
`;

const ImgMenuNameDiv = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  &:hover {
    opacity: ${props => (props.isSelected ? 1 : 0.7)}
    transition: ${props => props.isSelected && "opacity 0.3s ease"}
  }
`;
