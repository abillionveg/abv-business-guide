import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import Modal from "../../../../globalComponents/modal.js";
import Button from "../../../../globalComponents/button.js";

import Menu from "./local_components/menu.js";

import theme from "../../../../globalStyles.js";

import LogoCafeWhitePng from "../../../../assets/logo-cafe-white.png";
import LogoChipotleWhitePng from "../../../../assets/logo-chipotle-white.png";
import LogoEmpressWhitePng from "../../../../assets/logo-empress-white.png";
import LogoPizzaExpressWhitePng from "../../../../assets/logo-pizza-express-white.png";
import LogoPretWhitePng from "../../../../assets/logo-pret-white.png";

import LogoCafeGreyPng from "../../../../assets/logo-cafe-grey.png";
import LogoChipotleGreyPng from "../../../../assets/logo-chipotle-grey.png";
import LogoEmpressGreyPng from "../../../../assets/logo-empress-grey.png";
import LogoPizzaExpressGreyPng from "../../../../assets/logo-pizza-express-grey.png";
import LogoPretGreyPng from "../../../../assets/logo-pret-grey.png";

class BusinessCaseStudiesContainer extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired,
    businessNames: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
    const { copy } = this.props;

    let businessCaseStudiesWithLogo = [];
    businessCaseStudiesWithLogo = copy.businessCaseStudies.map(study => ({
      ...study,
      logoGrey: this.getLogo(study.company, "grey"),
      logoWhite: this.getLogo(study.company, "white")
    }));

    this.state = {
      activeMenuItem: businessCaseStudiesWithLogo[0],
      activeIndex: 0,
      modalIsVisible: false,
      businessCaseStudiesWithLogo: businessCaseStudiesWithLogo
    };
  }

  getLogo(company, color) {
    const { businessNames } = this.props;
    const isGrey = color === "grey";

    let logo;
    switch (company) {
      case businessNames.PIZZAEXPRESS:
        logo = isGrey ? LogoPizzaExpressGreyPng : LogoPizzaExpressWhitePng;
        break;
      case businessNames.CHIPOTLE:
        logo = isGrey ? LogoChipotleGreyPng : LogoChipotleWhitePng;
        break;
      case businessNames.EMPRESS:
        logo = isGrey ? LogoEmpressGreyPng : LogoEmpressWhitePng;
        break;
      case businessNames.CAFES:
        logo = isGrey ? LogoCafeGreyPng : LogoCafeWhitePng;
        break;
      case businessNames.PRET:
        logo = isGrey ? LogoPretGreyPng : LogoPretWhitePng;
        break;
      default:
        return;
    }
    return logo;
  }

  handleOnMenuItemChange(index) {
    const { businessCaseStudiesWithLogo } = this.state;
    this.setState({
      activeMenuItem: businessCaseStudiesWithLogo[index],
      activeIndex: index
    });
  }

  handleOnReadMoreClick() {
    this.setState({
      modalIsVisible: true
    });
  }

  handleModalClose(e) {
    this.setState({
      modalIsVisible: false
    });
  }

  renderModalContent() {
    const { activeMenuItem } = this.state;
    return (
      <div>
        <LogoDiv>
          <Img src={activeMenuItem.logoGrey} />
        </LogoDiv>
        {activeMenuItem.writeup.map((item, index) => (
          <p key={index}>
            <BulletPoint />
            {item}
          </p>
        ))}
      </div>
    );
  }

  render() {
    const { copy } = this.props;
    const {
      activeMenuItem,
      activeIndex,
      modalIsVisible,
      businessCaseStudiesWithLogo
    } = this.state;

    return (
      <BgWrapper>
        <Container>
          <Content>
            <H1>{copy.howDifferentBusinessesVeganize.title}</H1>
            <Divider />
            <SubheadingH2>
              {copy.howDifferentBusinessesVeganize.subtitle}
            </SubheadingH2>
            <MenuDiv>
              <Menu
                caseStudies={businessCaseStudiesWithLogo}
                onChange={index => this.handleOnMenuItemChange(index)}
                onClick={index => this.handleOnMenuItemChange(index)}
                selectedItem={activeIndex}
              />
            </MenuDiv>
            <CaseStudyHeader>
              <CaseStudyTitle>Case Study </CaseStudyTitle>
              <VerticalDivider> &#124; </VerticalDivider>
              <DividerSmall />
              <CompanyP>{activeMenuItem.company}</CompanyP>
            </CaseStudyHeader>
            <CaseStudyBlurb>{activeMenuItem.writeup[0]}</CaseStudyBlurb>
            <ButtonDiv onClick={() => this.handleOnReadMoreClick()}>
              <Button text={copy.readMore} bgColor={theme.colorGreen} />
            </ButtonDiv>
          </Content>
        </Container>
        <Modal
          isVisible={modalIsVisible}
          handleClose={e => this.handleModalClose(e)}
          breadcrumbItems={[
            copy.veganBusinessCaseStudies,
            activeMenuItem.company
          ]}
          moreStyles={{
            border: `5px solid ${theme.colorGreen}`,
            color: theme.colorGreen
          }}
        >
          <ModalContentDiv>{this.renderModalContent()}</ModalContentDiv>
        </Modal>
      </BgWrapper>
    );
  }
}

export default BusinessCaseStudiesContainer;

const ModalContentDiv = styled.div`
  margin-top: 84px;
`;

const BgWrapper = styled.div`
  background: white;
`;

const Container = styled.div`
  max-width: ${props => props.theme.maxContentWidth};
  margin: 0 auto;
  padding: 2em 0;
`;

const Content = styled.div`
  width: 85%;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  @media (min-width: 600px) {
  }
  p {
    text-align: center;
  }
`;

const H1 = styled.h1`
  color: black;
  text-align: center;
  font-size: 20px;

  @media (min-width: 600px) {
    width: 70%
    margin: 0 auto;
  }

  @media (min-width: 800px) {
    width: 50%;

    font-size: 24px;
  }
  @media (min-width: 900px) {
    font-size: 28px;
  }
  @media (min-width: 1200px) {
    font-size: 36px;
  }
`;

const SubheadingH2 = styled.h2`
  font-weight: 400;
  text-align: center;
  font-size: 16px;
  @media (min-width: 900px) {
    width: 55%;
    margin: 0 auto;
    font-size: 20px;
  }
  @media (min-width: 1200px) {
    font-size: 24px;
  }
`;

const Divider = styled.div`
  width: 30%;
  height: 4px;
  background: ${props => props.theme.colorGreen};
  margin: 0.5em auto;
  @media (min-width: 600px) {
    margin: 2em auto;
    width: 20%;
  }
  @media (min-width: 1200px) {
    width: 15%;
    height: 6px;
  }
`;

const DividerSmall = styled(Divider)`
  height: 2px;
  @media (min-width: 600px) {
    display: none;
  }
`;

const MenuDiv = styled.div`
  margin-top: 1em;
  margin-bottom: 1em;
  @media (min-width: 600px) {
    margin: 2em 0;
  }
`;

const CaseStudyHeader = styled.div`
  text-align: center;
  color: ${props => props.theme.colorGreen};
  @media (min-width: 600px) {
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

const CaseStudyTitle = styled.p`
  @media (min-width: 600px) {
    margin-right: 0.5em;
  }
`;

const VerticalDivider = styled.span`
  display: none;
  @media (min-width: 600px) {
    display: inline;
    margin-right: 0.5em;
    font-size: 24px;
  }
`;

const CompanyP = styled.p`
  font-weight: 800;
  font-size: 20px;
`;

const CaseStudyBlurb = styled.p`
  color: ${props => props.theme.colorGreen};
  @media (min-width: 600px) {
    // text-align: left !important;
  }
`;

const ButtonDiv = styled.div`
  text-align: center;
  @media (min-width: 600px) {
    // text-align: left;
  }
`;

const BulletPoint = styled.span`
  display: inline-block;
  height: 10px;
  width: 10px;
  margin-right: 2em;
  background-color: ${props => props.theme.colorGreen};
  transform: rotate(45deg);
`;

const LogoDiv = styled.div`
  width: 80px;
  margin: 2em auto;
  @media (min-width: 600px) {
    width: 120px;
  }
`;

const Img = styled.img`
  width: 100%;
`;
