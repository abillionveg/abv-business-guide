import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import NoAnimalTestingPng from "../../../../assets/no-animal-testing.png";
import NoDairyPng from "../../../../assets/no-dairy.png";
import NoHoneyPng from "../../../../assets/no-honey.png";
import NoLeatherPng from "../../../../assets/no-leather.png";
import NoMeatPng from "../../../../assets/no-meat.png";
import ArrowForwardPng from "../../../../assets/arrows-forward.png";
import ArrowBackwardPng from "../../../../assets/arrows-backward.png";

class WhatIsVeganFoodContainer extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired
  };

  renderIcons() {
    const Icons = [
      {
        name: NoLeatherPng,
        alt: "No leather"
      },
      {
        name: NoAnimalTestingPng,
        alt: "No animal testing"
      },
      {
        name: NoHoneyPng,
        alt: "No honey"
      },
      {
        name: NoMeatPng,
        alt: "No meat"
      },
      {
        name: NoDairyPng,
        alt: "No dairy"
      }
    ];

    return (
      <IconsContainer>
        {Icons.map((icon, index) => (
          <IconDiv key={index}>
            <IconImg src={icon.name} alt={icon.alt} title={icon.alt} />
            <IconP>{icon.alt}</IconP>
          </IconDiv>
        ))}
      </IconsContainer>
    );
  }

  render() {
    const { copy } = this.props;
    return (
      <BgWrapper>
        <Container>
          <TextContentDiv>
            <HeaderDiv>
              <ImgDiv>
                <Img src={ArrowForwardPng} />
              </ImgDiv>
              <H1>{copy.whatIsVeganFood.title}</H1>
              <ImgDiv>
                <Img src={ArrowBackwardPng} />
              </ImgDiv>
            </HeaderDiv>
            <H2>{copy.whatIsVeganFood.body}</H2>
          </TextContentDiv>
          {this.renderIcons()}
        </Container>
      </BgWrapper>
    );
  }
}

export default WhatIsVeganFoodContainer;

const BgWrapper = styled.div`
  background: ${props => props.theme.colorGreen};
  width: 100vw;
`;

const Container = styled.div`
  max-width: ${props => props.theme.maxContentWidth};
  margin: 0 auto;
  padding: 1.5em;
`;

const TextContentDiv = styled.div`
  padding-bottom: 1em;
  margin-top: 0;
  text-align: center;
  color: white;
  @media (min-width: 600px) {
    width: 80%;
    margin: 0 auto;
    padding-left: 0;
    padding-right: 0;
    padding-bottom: 3em;
  }
  @media (min-width: 1200px) {
    width: 60%;
    margin: 0 auto;
    padding-left: 0;
    padding-right: 0;
    padding-bottom: 5em;
  }
`;

const HeaderDiv = styled.div`
  display: flex;
  justify-content: center;
`;

const ImgDiv = styled.div`
  width: 10%;
  display: flex;
  align-items: center;
  padding: 5px;
`;

const Img = styled.img`
  width: 100%;
  max-width: 100%;
`;

const H1 = styled.h1`
  color: white;
  font-size: 20px;
  text-align: center;
  text-transform: uppercase;
  font-weight: 700;
  @media (min-width: 600px) {
    font-size: 24px;
  }
  @media (min-width: 900px) {
    padding-left: 10px;
    padding-right: 10px;

    font-size: 36px;
  }
  @media (min-width: 1800px) {
    font-size: 44px;
  }
`;

const H2 = styled.h2`
  margin: 0;
  color: white;
  font-size: 16px;
  text-align: center;
  font-weight: 400;
  @media (min-width: 600px) {
    font-size: 20px;
  }
  @media (min-width: 900px) {
    font-size: 28px;
  }
  @media (min-width: 1800px) {
    margin: 1em 0;
    font-size: 36px;
  }
`;

const IconsContainer = styled.div`
  display: flex;
  justify-content: center;
  @media (min-width: 900px) {
    width: 80%;
    margin: 0 auto;
  }
`;

const IconDiv = styled.div`
  text-align: center;
`;

const IconImg = styled.img`
  max-width: 70%;
`;

const IconP = styled.p`
  color: white;
  font-size: 12px;
  font-weight: 400;
  @media (min-width: 900px) {
    font-size: 20px;
    font-weight: bold;
  }
`;
