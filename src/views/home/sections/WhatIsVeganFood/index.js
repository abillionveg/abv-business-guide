import React, { Component } from "react";
import PropTypes from "prop-types";

import WhatIsVeganFoodContainer from "./WhatIsVeganFoodContainer.js";

class WhatIsVeganFoodSection extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired
  };

  render() {
    const { copy } = this.props;
    return <WhatIsVeganFoodContainer copy={copy} />;
  }
}

export default WhatIsVeganFoodSection;
