import React, { Component } from "react";
import PropTypes from "prop-types";

import GetStartedYourselfContainer from "./GetStartedYourselfContainer.js";

class GetStartedYourselfSection extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired
  };

  render() {
    const { copy } = this.props;
    return <GetStartedYourselfContainer copy={copy} />;
  }
}

export default GetStartedYourselfSection;
