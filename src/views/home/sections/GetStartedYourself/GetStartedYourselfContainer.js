import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import Button from "../../../../globalComponents/button.js";

import theme from "../../../../globalStyles.js";

import CheatSheetPng from "../../../../assets/cheat-sheet.png";
import FoodBiblePng from "../../../../assets/food-bible.png";
import PlaceholderPng from "../../../../assets/placeholder.png";

class GetStartedYourselfContainer extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired
  };

  render() {
    const { copy } = this.props;
    const logos = [CheatSheetPng, FoodBiblePng, PlaceholderPng];

    return (
      <BgWrapper>
        <Container>
          <Content>
            <H1>{copy.getStartedYourself}</H1>
            <Divider />
            <SubheadingH2>{copy.getStartedYourselfSubheading}</SubheadingH2>
            <Steps>
              {copy.getStartedYourselfContent.map((item, index) => (
                <Step key={index} isFirstChild={index === 0}>
                  <LogoDiv>
                    <a
                      href={item.link}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <Img src={logos[index]} />
                    </a>
                  </LogoDiv>
                  <StepTitle>{item.title}</StepTitle>
                  <DividerSmall />
                  <StepBlurb>{item.blurb}</StepBlurb>
                  <a href={item.link} target="_blank" rel="noopener noreferrer">
                    <Button text={copy.view} bgColor={theme.colorPurple} />
                  </a>
                </Step>
              ))}
            </Steps>
          </Content>
        </Container>
      </BgWrapper>
    );
  }
}

export default GetStartedYourselfContainer;

const BgWrapper = styled.div`
  background: ${props => props.theme.colorLightPurple};
`;

const Container = styled.div`
  max-width: ${props => props.theme.maxContentWidth};
  margin: 0 auto;
`;

const Content = styled.div`
  width: 85%;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  padding: 2em 0;
  p {
    text-align: center;
  }
`;

const H1 = styled.h1`
  color: black;
  text-align: center;
  font-size: 20px;

  @media (min-width: 600px) {
    width: 70%
    margin: 0 auto;
  }

  @media (min-width: 800px) {
    width: 50%;

    font-size: 24px;
  }
  @media (min-width: 900px) {
    font-size: 28px;
  }
  @media (min-width: 1200px) {
    font-size: 36px;
  }
`;

const SubheadingH2 = styled.h2`
  font-weight: 400;
  text-align: center;
  font-size: 16px;
  @media (min-width: 900px) {
    width: 55%;
    margin: 0 auto;
    font-size: 20px;
  }
  @media (min-width: 1200px) {
    font-size: 24px;
  }
`;

const Divider = styled.div`
  width: 30%;
  height: 2px;
  background: ${props => props.theme.colorPurple};
  margin: 0.5em auto;
  @media (min-width: 600px) {
    height: 4px;
    margin: 2em auto;
    width: 20%;
  }
  @media (min-width: 1200px) {
    width: 15%;
    height: 6px;
  }
`;

const DividerSmall = styled(Divider)`
  height: 3px !important;
  margin: 0.4em auto !important;
`;

const Steps = styled.div`
  flex-direction: column;
  margin-top: 2em;
  display: flex;
  @media (min-width: 600px) {
    flex-direction: row;
  }
`;

const Step = styled.div`
  flex: 1;
  text-align: center;
  a {
    text-decoration: none;
  }
  margin-top: ${props => (props.isFirstChild ? 0 : "3em")};
  @media (min-width: 600px) {
    margin-top: 0;
  }
`;

const StepTitle = styled.p`
  margin-bottom: 0;
  font-weight: 600;
`;
const StepBlurb = styled.p`
  color: ${props => props.theme.colorLightGrey};
  @media (min-width: 600px) {
    min-height: 200px;
  }
  @media (min-width: 900px) {
    min-height: 140px;
  }
`;

const LogoDiv = styled.div`
  width: 80px;
  height: 80px;
  margin: 1em auto;
  display: flex;
  align-items: center;
  cursor: pointer;
  &:hover {
    opacity: 0.7;
  }
  transition: opacity 0.3s ease;
`;

const Img = styled.img`
  width: 100%;
  max-height: 100%;
`;
