import React, { Component } from "react";
import styled from "styled-components";

import LandingPageBgPng from "../../../../assets/landing-page-bg.png";
import LandingPageBgMobilePng from "../../../../assets/landing-page-bg-mobile.png";
import LandingPageHeaderPng from "../../../../assets/landing-page-header.png";

class HeroContainer extends Component {
  render() {
    return (
      <Container>
        <HeroDiv>
          <HeaderDiv>
            <Img src={LandingPageHeaderPng} alt="how to veganize your menu" />
          </HeaderDiv>
        </HeroDiv>
      </Container>
    );
  }
}

export default HeroContainer;

const Container = styled.div`
  height: 50vh;
  @media (min-width: 600px) {
    height: 60vh;
  }
  @media (min-width: 1200px) {
    height: 90vh;
  }
`;

const HeroDiv = styled.div`
  background-image: url(${LandingPageBgMobilePng});
  height: 100%;
  width: 100%;
  background-size: cover;
  background-position: top center;
  @media (min-width: 1200px) {
    background-image: url(${LandingPageBgPng});
    background-repeat: no-repeat;
  }
`;

const HeaderDiv = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Img = styled.img`
  width: 70%;
  margin-top: 30px;
  @media (min-width: 900px) {
    width: 50%;
    margin-top: 50px;
  }
`;
