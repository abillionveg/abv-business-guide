import React, { Component } from "react";
import HeroContainer from "./HeroContainer.js";

class HeroSection extends Component {
  render() {
    return <HeroContainer />;
  }
}

export default HeroSection;
