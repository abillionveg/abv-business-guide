import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import Ticker from "./local_components/Ticker.js";

class OverviewContainer extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired
  };

  render() {
    const { copy } = this.props;
    return (
      <BgWrapper>
        <Ticker copy={copy} />
        <Container>
          <ContentDiv>
            <H1Desktop>{copy.overviewText}</H1Desktop>
            <H1Mobile
              dangerouslySetInnerHTML={{ __html: `${copy.overviewText2Para}` }}
            />
          </ContentDiv>
        </Container>
      </BgWrapper>
    );
  }
}

export default OverviewContainer;

const BgWrapper = styled.div`
  background: ${props => props.theme.colorOrange};
  width: 100vw;
  & .ticker {
    background-color: ${props => props.theme.colorDarkOrange} !important;
  }
`;

const Container = styled.div`
  max-width: ${props => props.theme.maxContentWidth};
  margin: 0 auto;
`;

const ContentDiv = styled.div`
  padding: 2em 1.5em;
  margin-top: 0;
  text-align: center;
  color: white;
  @media (min-width: 600px) {
    width: 80%;
    margin: 0 auto;
    padding-left: 0;
    padding-right: 0;
  }
  @media (min-width: 1200px) {
    width: 60%;
    margin: 0 auto;
    padding-left: 0;
    padding-right: 0;
  }
`;

const H1Desktop = styled.h1`
  display: none;
  margin-top: 0;
  @media (min-width: 1200px) {
    font-weight: 400;
    font-size: 28px;
    display: block;
  }
  @media (min-width: 1800px) {
    font-size: 32px;
  }
`;
const H1Mobile = styled.h1`
  margin-top: 0;
  font-weight: 400;
  font-size: 16px;
  @media (min-width: 600px) {
    font-size: 24px;
  }
  @media (min-width: 1024px) {
    font-size: 32px;
  }
  @media (min-width: 1200px) {
    display: none;
  }
`;
