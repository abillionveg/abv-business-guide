import React, { Component } from "react";
import PropTypes from "prop-types";

import OverviewContainer from "./OverviewContainer.js";

class OverviewSection extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired
  };

  render() {
    const { copy } = this.props;
    return <OverviewContainer copy={copy} />;
  }
}

export default OverviewSection;
