import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import _range from "lodash/range";

import StarFilledPng from "../../../assets/quiz/star-filled.png";
import StarEmptyPng from "../../../assets/quiz/star-empty.png";

class ScorecardItem extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    pointsForItem: PropTypes.number.isRequired
  };

  render() {
    const { name, pointsForItem } = this.props;
    const maxNumberOfStarts = 5;
    const emptyStars = maxNumberOfStarts - pointsForItem;
    return (
      <Container>
        <p>{name}:</p>
        <StarsContainer>
          {_range(pointsForItem).map((point, index) => (
            <StarDiv key={`point-${index}`}>
              <Image src={StarFilledPng} alt="star-filled" />
            </StarDiv>
          ))}
          {_range(emptyStars).map((empty, index) => (
            <StarDiv key={`empty-${index}`}>
              <Image src={StarEmptyPng} alt="star-empty" />
            </StarDiv>
          ))}
        </StarsContainer>
      </Container>
    );
  }
}

export default ScorecardItem;

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  p {
    text-transform: uppercase;
    width: 30%;
    margin: 0 0.1em;
    text-align: right;
    flex: 2;
    margin-right: 0.5em;
    @media (min-width: 900px) {
      font-size: 20px;
    }
  }
`;

const StarsContainer = styled.div`
  flex: 2;
  display: flex
  margin-left: 0.5em
`;

const StarDiv = styled.div`
  width: 20px;
  height: 20px;
  @media (min-width: 900px) {
    width: 30px;
    height: 30px;
  }
  margin: 0.1em;
`;

const Image = styled.img`
  width: 100%;
`;
