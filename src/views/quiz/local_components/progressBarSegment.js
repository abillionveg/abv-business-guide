import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import theme from "../../../globalStyles";

class ProgressBarSegment extends Component {
  static propTypes = {
    isActive: PropTypes.bool.isRequired,
    isCompleted: PropTypes.bool.isRequired,
    isLastSegment: PropTypes.bool.isRequired,
    index: PropTypes.number
  };

  getCircleBgColor() {
    const { isCompleted } = this.props;
    let color = "";
    if (!isCompleted) {
      color = theme.colorGrey;
    } else {
      color = theme.colorOrange;
    }
    return color;
  }

  getBarBgColor() {
    const { isCompleted } = this.props;
    let color = "";
    if (!isCompleted) {
      color = theme.colorGrey;
    } else {
      color = theme.colorOrange;
    }
    return color;
  }
  render() {
    const { isActive, isLastSegment } = this.props;

    return (
      <Container isLastSegment={isLastSegment}>
        <Circle bgColor={this.getCircleBgColor()} isActive={isActive} />
        <CircleActiveRing isActive={isActive}>
          <CircleActive isActive={isActive} />
        </CircleActiveRing>
        <Bar bgColor={this.getBarBgColor()} isLastSegment={isLastSegment} />
      </Container>
    );
  }
}

export default ProgressBarSegment;

const Container = styled.div`
  display: flex;
  align-items: center;
  width: ${props => !props.isLastSegment && "25%"};
`;

const Circle = styled.div`
  background: ${props => props.bgColor}
  width: 12px;
  height: 12px;
  border-radius: 50%;
  display: ${props => props.isActive && "none"}
`;

const CircleActiveRing = styled.div`
 position: relative;
  display: ${props => (!props.isActive ? "none" : "block")}
  width: 18px;
  height: 18px;
  background: white;
  border: 1px solid ${props => props.theme.colorOrange}
  border-radius: 50%;
`;

const CircleActive = styled.div`
  position: absolute;
  display: ${props => (!props.isActive ? "none" : "block")}
  background: ${props => props.theme.colorOrange};
  width: 12px;
  height: 12px;
  border-radius: 50%;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

const Bar = styled.div`
  flex-grow: 1;
  height: 3px;
  background: ${props => props.bgColor};
  display: ${props => props.isLastSegment && "none"};
`;
