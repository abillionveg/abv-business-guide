import React, { Component } from "react";
import PropTypes from "prop-types";

import QuizContainer from "./QuizContainer.js";

class QuizSection extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired,
    externalLinks: PropTypes.object.isRequired,
    routes: PropTypes.object.isRequired
  };

  render() {
    const { copy, externalLinks, routes } = this.props;
    return (
      <QuizContainer
        copy={copy}
        externalLinks={externalLinks}
        routes={routes}
      />
    );
  }
}

export default QuizSection;
