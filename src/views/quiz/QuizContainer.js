import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import _find from "lodash/find";
import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton
} from "react-share";

import Option from "./local_components/option.js";
import ProgressBarSegment from "./local_components/progressBarSegment.js";
import ScorecardItem from "./local_components/scorecardItem.js";

import Button from "../../globalComponents/button.js";

import theme from "../../globalStyles";

import AbvLogoWhitePng from "../../assets/abv-logo-white.png";

import LandingPageBgPng from "../../assets/quiz/landing-page-bg.png";
import MobileLandingPageBgPng from "../../assets/quiz/mobile-landing-page-bg.png";
import LandingPageHeaderPng from "../../assets/quiz/landing-page-header.png";
import MobileLandingPageHeaderPng from "../../assets/quiz/mobile-landing-page-header.png";
import BackgroundPng from "../../assets/quiz/background.png";
import MobileBackgroundPng from "../../assets/quiz/mobile-background.png";
import TrophyPng from "../../assets/quiz/trophy.png";
import SocialFacebookGreenPng from "../../assets/quiz/social-facebook-green.png";
import SocialLinkedinGreenPng from "../../assets/quiz/social-linkedin-green.png";
import SocialTwitterGreenPng from "../../assets/quiz/social-twitter-green.png";
import SharePng from "../../assets/quiz/share.png";
import SocialFacebookPng from "../../assets/quiz/social-facebook.png";
import SocialLinkedinPng from "../../assets/quiz/social-linkedin.png";
import SocialTwitterPng from "../../assets/quiz/social-twitter.png";
import ArrowPng from "../../assets/quiz/arrow.png";
import ArrowBackPng from "../../assets/quiz/arrow-back.png";

class QuizContainer extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired,
    externalLinks: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      startQuiz: false,
      activePageIndex: 0,
      slideLeftDistance: 0,
      quizSections: this.props.copy.quizSections,
      showQuizLinkDropdown: false
    };
  }

  UNSAFE_componentWillMount() {
    document.addEventListener(
      "mousedown",
      this.handleShareQuizLinkClick,
      false
    );
  }

  componentWillUnmount() {
    document.removeEventListener(
      "mousedown",
      this.handleShareQuizLinkClick,
      false
    );
  }

  handleOnStartQuizClick() {
    this.setState({
      startQuiz: true
    });
  }

  handleOnNextQuestionClick(index) {
    const { activePageIndex, quizSections } = this.state;

    const optionsForActivePage = quizSections[activePageIndex].options;

    let atLeastOneOptionSelected = optionsForActivePage.some(function(option) {
      return option.isSelected === true;
    });

    if (atLeastOneOptionSelected === true) {
      this.setState({
        activePageIndex: activePageIndex + 1,
        slideLeftDistance: (index + 1) * 100
      });
    }
  }

  handleOnBackClick(index) {
    const { activePageIndex } = this.state;

    this.setState({
      activePageIndex: activePageIndex - 1,
      slideLeftDistance: (index - 1) * 100
    });
  }

  getNextButtonStyles() {
    const { activePageIndex, quizSections } = this.state;
    const optionsForActivePage =
      !!quizSections[activePageIndex] && quizSections[activePageIndex].options;

    let result = {};

    if (!!optionsForActivePage) {
      let atLeastOneOptionSelected = optionsForActivePage.some(function(
        option
      ) {
        return option.isSelected === true;
      });

      if (atLeastOneOptionSelected) {
        result = { background: theme.colorGreen, padding: "0 1em" };
      } else {
        result = {
          background: theme.colorLightestGrey,
          color: theme.colorGrey,
          padding: "0 1em",
          cursor: "not-allowed",
          "&:hover": {
            opacity: 1
          }
        };
      }
    }

    return result;
  }

  handleOptionClick(optionName, sectionTitle) {
    const { copy } = this.props;
    if (optionName !== copy.noneOfTheAbove) {
      this.setState(prevState => ({
        quizSections: prevState.quizSections.map(
          section =>
            section.title === sectionTitle
              ? {
                  ...section,
                  options: section.options.map(
                    option =>
                      option.name === optionName
                        ? {
                            ...option,
                            isSelected: !option.isSelected
                          }
                        : option.name === copy.noneOfTheAbove
                          ? { ...option, isSelected: false }
                          : option
                  )
                }
              : section
        )
      }));
    } else {
      this.setState(prevState => ({
        quizSections: prevState.quizSections.map(
          section =>
            section.title === sectionTitle
              ? {
                  ...section,
                  options: section.options.map(
                    option =>
                      option.name === optionName
                        ? { ...option, isSelected: !option.isSelected }
                        : { ...option, isSelected: false }
                  )
                }
              : section
        )
      }));
    }
  }

  renderSplashScreen() {
    const { copy } = this.props;
    const { startQuiz } = this.state;

    return (
      <MainContainer startQuiz={startQuiz}>
        {this.renderNavBar()}

        <Content ref={content => (this.content = content)}>
          <ImgDesktop
            src={LandingPageHeaderPng}
            alt="how vegan-friendly are you?"
          />
          <ImgMobile
            src={MobileLandingPageHeaderPng}
            alt="how vegan-friendly are you?"
          />

          <div onClick={() => this.handleOnStartQuizClick()}>
            <Button
              text={copy.startQuiz}
              bgColor={theme.colorBlue}
              isUppercase
              moreStyles={{
                fontSize: "20px !important",
                height: "50px"
              }}
            />
          </div>
        </Content>
      </MainContainer>
    );
  }

  renderQuizPages() {
    const { copy } = this.props;
    const {
      startQuiz,
      activePageIndex,
      slideLeftDistance,
      quizSections
    } = this.state;

    return (
      <React.Fragment>
        {startQuiz && (
          <FixedBackgroundDiv>
            {this.renderNavBar()}

            <QuizMegaWrapper
              slideLeftDistance={slideLeftDistance}
              showResults={activePageIndex === quizSections.length}
              ref={content => (this.content = content)}
            >
              {!!quizSections &&
                quizSections.map((section, index) => (
                  <QuizPage
                    isActivePage={index === activePageIndex}
                    index={index}
                    key={index}
                  >
                    <QuizPageContainer>
                      <H1>{copy.howVeganFriendlyAreYou}</H1>
                      <QuizContent>
                        <ProgressBarDiv>
                          {quizSections.map((section, index) => (
                            <ProgressBarSegment
                              key={`progress-bar-${index}`}
                              isLastSegment={index === quizSections.length - 1}
                              isActive={index === activePageIndex}
                              index={index}
                              isCompleted={index < activePageIndex}
                            />
                          ))}
                        </ProgressBarDiv>
                        <Section>
                          <SectionTop>
                            <H2>
                              {index + 1}. {section.title}
                            </H2>
                            <H3>{section.blurb}</H3>

                            <p>{copy.selectAllThatApply}</p>
                          </SectionTop>
                          {!!section &&
                            !!section.options &&
                            section.options.map((option, index) => (
                              <Option
                                key={index}
                                option={option}
                                onClick={(option, sectionTitle) =>
                                  this.handleOptionClick(option, sectionTitle)
                                }
                                sectionTitle={section.title}
                                isSelected={
                                  !!option.isSelected
                                    ? option.isSelected
                                    : false
                                }
                              />
                            ))}
                        </Section>
                      </QuizContent>
                      <ButtonsDiv>
                        {activePageIndex !== 0 && (
                          <ButtonDiv
                            onClick={() => this.handleOnBackClick(index)}
                          >
                            <Button
                              text={copy.back}
                              moreStyles={{
                                background: theme.colorGreen,
                                padding: "0 1em"
                              }}
                              isUppercase
                              icon={ArrowBackPng}
                              iconPosition="left"
                            />
                          </ButtonDiv>
                        )}
                        <ButtonDiv
                          onClick={() => this.handleOnNextQuestionClick(index)}
                        >
                          <Button
                            text={copy.next}
                            moreStyles={this.getNextButtonStyles()}
                            isUppercase
                            icon={ArrowPng}
                            iconPosition="right"
                          />
                        </ButtonDiv>
                      </ButtonsDiv>
                    </QuizPageContainer>
                  </QuizPage>
                ))}
            </QuizMegaWrapper>
          </FixedBackgroundDiv>
        )}
      </React.Fragment>
    );
  }

  getPointsForSection(section) {
    const { copy } = this.props;
    let result = 0;
    section.options.forEach(option => {
      if (option.name !== copy.noneOfTheAbove && option.isSelected === true) {
        result++;
      }
    });
    return result;
  }

  renderResultsHeader() {
    const { copy } = this.props;
    return (
      <React.Fragment>
        <ResultsH1>{copy.howVeganFriendlyAreYou}</ResultsH1>
      </React.Fragment>
    );
  }

  renderResultTitleMessage(computedResultsContent) {
    return (
      <React.Fragment>
        <ResultTitle>
          {!!computedResultsContent && computedResultsContent.title}
        </ResultTitle>
        <ResultP>
          {!!computedResultsContent && computedResultsContent.message}
        </ResultP>
      </React.Fragment>
    );
  }

  renderTrophyAndStars(totalPoints) {
    const { copy } = this.props;
    const { quizSections } = this.state;
    return (
      <TrophyStarsDiv>
        <TrophyDiv>
          <Img src={TrophyPng} alt="trophy" />
          <PointsValue>{totalPoints}</PointsValue>
          <PointsP>{totalPoints === 1 ? copy.point : copy.points}</PointsP>
        </TrophyDiv>
        <ScorecardDiv>
          {quizSections.map((section, index) => (
            <ScorecardItem
              key={index}
              name={section.title}
              pointsForItem={this.getPointsForSection(section)}
            />
          ))}
        </ScorecardDiv>
      </TrophyStarsDiv>
    );
  }

  renderCTA(computedResultsContent) {
    const { externalLinks } = this.props;
    return (
      <CTADiv>
        <p>{!!computedResultsContent && computedResultsContent.cta}</p>
        <a
          href={externalLinks.claimYourBusiness}
          target="_blank"
          rel="noopener noreferrer"
        >
          <Button
            text={!!computedResultsContent && computedResultsContent.button}
            bgColor={theme.colorGreen}
          />
        </a>
      </CTADiv>
    );
  }

  renderShareYourResults(totalPoints) {
    const { copy, externalLinks, routes } = this.props;
    const quizLink = `${externalLinks.businessGuideShareLink}${routes.quiz}`;

    const socials = [
      {
        icon: SocialFacebookGreenPng,
        shareButton: FacebookShareButton,
        additionalProps: {
          quote: copy.shareResultsText(totalPoints, quizLink, true)
        }
      },
      {
        icon: SocialLinkedinGreenPng,
        shareButton: LinkedinShareButton
      },
      {
        icon: SocialTwitterGreenPng,
        shareButton: TwitterShareButton,
        additionalProps: {
          title: copy.shareResultsText(totalPoints, quizLink)
        }
      }
    ];

    return (
      <ShareYourResultsDiv>
        <p>{copy.shareYourResults}:</p>
        <ShareIconsDiv>
          {socials.map(social => (
            <social.shareButton url={quizLink} {...social.additionalProps}>
              <ShareIconDiv>
                <ShareIconImg src={social.icon} />
              </ShareIconDiv>
            </social.shareButton>
          ))}
        </ShareIconsDiv>
      </ShareYourResultsDiv>
    );
  }

  renderResultsPage() {
    const { copy } = this.props;
    const { activePageIndex, quizSections } = this.state;

    let totalPoints = 0;

    quizSections.forEach(section =>
      section.options.forEach(option => {
        if (option.name !== copy.noneOfTheAbove && option.isSelected === true) {
          totalPoints++;
        }
      })
    );

    let computedResultsContent = {};
    computedResultsContent = _find(copy.computedResults, function(obj) {
      return (
        totalPoints >= obj.pointsLowerBound &&
        totalPoints <= obj.pointsUpperBound
      );
    });

    return (
      <ResultsPageContainer
        showResults={activePageIndex === quizSections.length}
      >
        <FixedBackgroundDiv>
          {this.renderNavBar()}
          <ResultsPage ref={content => (this.content = content)}>
            {this.renderResultsHeader()}

            <ResultsContent>
              <ResultsSection>
                {this.renderResultTitleMessage(computedResultsContent)}
                {this.renderTrophyAndStars(totalPoints)}
                {this.renderCTA(computedResultsContent)}
                {this.renderShareYourResults(totalPoints)}
              </ResultsSection>
            </ResultsContent>
          </ResultsPage>
        </FixedBackgroundDiv>
      </ResultsPageContainer>
    );
  }

  handleShareQuizLinkClick = e => {
    console.log("click");
    if (
      this.content.contains(e.target) &&
      this.state.showQuizLinkDropdown === true
    ) {
      this.setState({ showQuizLinkDropdown: false });
    }
  };

  handleOnShareQuizLinkIconClick() {
    this.setState({
      showQuizLinkDropdown: !this.state.showQuizLinkDropdown
    });
  }

  renderNavBar() {
    const { externalLinks, routes } = this.props;
    const quizLink = `${externalLinks.businessGuideShareLink}${routes.quiz}`;

    const socials = [
      {
        icon: SocialFacebookPng,
        shareButton: FacebookShareButton
      },
      {
        icon: SocialLinkedinPng,
        shareButton: LinkedinShareButton
      },
      {
        icon: SocialTwitterPng,
        shareButton: TwitterShareButton
      }
    ];

    const { showQuizLinkDropdown } = this.state;
    return (
      <NavBarContainer>
        <LogoDiv>
          <Img src={AbvLogoWhitePng} alt="abillionveg-logo" />
        </LogoDiv>
        <ShareQuizLinkDiv onClick={() => this.handleOnShareQuizLinkIconClick()}>
          <ShareQuizLinkIconDiv>
            <Img src={SharePng} alt="share" />
          </ShareQuizLinkIconDiv>
          {!!showQuizLinkDropdown && (
            <ShareQuizLinkDropdown
              ref={shareDropdown => (this.shareDropdown = shareDropdown)}
            >
              {socials.map((social, index) => (
                <social.shareButton
                  key={index}
                  url={quizLink}
                  {...social.additionalProps}
                  style={{ height: "50px" }}
                >
                  <ShareIconDiv>
                    <ShareIconImg src={social.icon} />
                  </ShareIconDiv>
                </social.shareButton>
              ))}
            </ShareQuizLinkDropdown>
          )}
        </ShareQuizLinkDiv>
      </NavBarContainer>
    );
  }

  render() {
    return (
      <Container>
        {this.renderSplashScreen()}
        {this.renderQuizPages()}
        {this.renderResultsPage()}
      </Container>
    );
  }
}

export default QuizContainer;

const Container = styled.div`
  min-height: 100vh;
`;

const NavBarContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  color: white;
  background: ${props => props.theme.colorOrange};
  height: 50px;
  // position: fixed;
  width: 100%;
  z-index: 1;
`;

const LogoDiv = styled.div`
  width: 100px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-left: 1em;
`;

const ShareQuizLinkDiv = styled.div`
  background: white;
  height: 100%;
  width: 50px;
  position: relative;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-right: 1em;
  @media (min-width: 900px) {
    margin-right: 30px;
  }
  box-sizing: border-box;
  border: 2px solid white;
  &:hover {
    border: 2px solid ${props => props.theme.colorOrange};
    box-sizing: border-box;
  }
  transition: border 0.3s ease;
  &:focus {
    -webkit-appearance: none;
    outline: none !important;
  }
`;

const ShareQuizLinkIconDiv = styled.div`
  width: 20px;
  display: flex;
  align-items: center;
  justify-content: center;

  }
`;

const ShareQuizLinkDropdown = styled.div`
  background: ${props => props.theme.colorOrange};
  z-index: 2
  width: 50px;
  position: absolute;
  top: 48px;
  div {
    display: flex;
    justify-content: center;
    align-items: center;
    margin: 0;
    &:focus {
      outline-width: 0px !important;
      outline: none !important;
    }
    &:hover img {
      height: 120%;
    }
  }
`;

const MainContainer = styled.div`
  background-image: ${props =>
    props.startQuiz ? "none !important" : `url(${MobileLandingPageBgPng})`};
  height: 100%;
  width: 100%;
  background-size: cover;
  background-position: top center;
  position: fixed;
  overflow-y: scroll;

  @media (min-width: 1200px) {
    background-image: ${props =>
      props.startQuiz ? "none !important" : `url(${LandingPageBgPng})`};
    background-repeat: no-repeat;
  }
  display: ${props => props.startQuiz && "none !important"};
`;

const Content = styled.div`
  height: 100%
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const ImgDesktop = styled.img`
  display: none;
  @media (min-width: 1200px) {
    display: block;
    width: 60%;
    margin-top: 12%;
    margin-bottom: 3em%;
  }
`;

const ImgMobile = styled.img`
  width: 75%;
  margin-top: 20%;
  margin-bottom: 10%;

  @media (min-width: 600px) {
    width: 60%;
    margin-top: 20%;
  }
  @media (min-width: 900px) {
    width: 55%;
    margin-top: 10%;
  }
  @media (min-width: 1000px) {
    width: 45%;
  }
  @media (min-width: 1200px) {
    display: none;
  }
`;

const FixedBackgroundDiv = styled.div`
  position: fixed;
  width: 100%;
  top: 0;
  bottom: 0;
  overflow-y: scroll;
  background-image: url(${MobileBackgroundPng});
  @media (min-width: 1200px) {
    background-image: url(${BackgroundPng});
  }
`;

const QuizMegaWrapper = styled.div`
  margin-left: ${props => `-${props.slideLeftDistance}vw`};
  transition: margin-left 0.5s ease;
  display: ${props => (props.showResults & "none": "flex")};
`;

const QuizPage = styled.div`
  display: block;
  margin-left: ${props => `${props.index * 100}vw`};
  height: 100%;
  width: 100%;
  background-size: cover;
  background-position: top center;
  position: fixed;
  overflow-y: scroll;
`;

const QuizPageContainer = styled.div`
  padding: 1em 0.5em 5em 0.5em;
  display: ${props => (props.showResults & "none": "flex")};
  @media (min-width: 600px) {
    padding: 1em;
  }
`;

const ResultsPage = styled(QuizPage)``;

const ResultsPageContainer = styled.div`
  display: ${props => (props.showResults ? "flex" : "none")};
`;

const QuizContent = styled.div`
  width: 90%;
  margin: 0 auto;

  @media (min-width: 600px) {
    width: 80%;
  }
`;

const ResultsContent = styled(QuizContent)`
  margin-bottom: 2em;
`;

const ResultsSection = styled.div`
  background: white;
  padding: 1em;

  @media (min-width: 900px) {
    padding: 2em 8em;
    width: 70%;
    margin: 0 auto;
  }
`;

const ResultTitle = styled.h2`
  font-size: 16px;
  text-align: center;
  color: ${props => props.theme.colorOrange};
  @media (min-width: 900px) {
    font-size: 32px;
  }
`;

const ResultP = styled.p`
  font-size: 16px;
  @media (min-width: 600px) {
    font-size: 20px;
  }
  text-align: center;
  color: ${props => props.theme.colorBlack};
  line-height: 30px;
`;

const ProgressBarDiv = styled.div`
  display: flex;
  padding: 1em;
`;

const Section = styled.div`
  background: white;
  padding: 1em;
  @media (min-width: 600px) {
    padding: 0.5em 2em;
  }
`;

const SectionTop = styled.div`
  justify-content: space-between
  flex-direction: column;
  align-items: flex-start;

  @media(min-width: 600px){
    flex-direction: row;
    align-items: center;
    margin-bottom: 1em;
  }
   p{
     font-size: 14px;
     margin-top: 0;
     @media(min-width: 600px){
       margin-top: 1em;
       margin-bottom: 1em;
     }
   }
`;

const H3 = styled.h3`
  font-size: 16px !important
  font-weight: 400;
`;

const H2 = styled.h2`
  color: ${props => props.theme.colorGreen};
  margin: 0;
  font-size: 16px;
  @media (min-width: 600px) {
    font-size: 24px;
  }
`;

const H1 = styled.h1`
  color: ${props => props.theme.colorGreen};
  text-transform: uppercase;
  font-size: 16px;
  text-align: center;
  margin-top: 4em;

  @media (min-width: 600px) {
    font-size: 28px;
    margin-top: 3em;
  }
  @media (min-width: 900px) {
    font-size: 36px;
    margin-top: 2em;
  }
  @media (min-width: 1200px) {
    font-size: 40px;
  }
  @media (min-width: 1800px) {
    font-size: 44px;
  }
`;

const ResultsH1 = styled.h1`
  color: ${props => props.theme.colorGreen};
  text-transform: uppercase;
  font-size: 16px;
  text-align: center;
  margin-top: 4em;
  @media (min-width: 600px) {
    font-size: 36px !important;
    margin-top: 2em;
  }
`;

const ButtonDiv = styled.div`
  text-align: center;
  margin: 0 0.5em;
`;

const TrophyStarsDiv = styled.div`
  @media (min-width: 900px) {
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 0 auto;
    padding: 2em 0;
  }
  @media (min-width: 1200px) {
  }
  @media (min-width: 1800px) {
  }
`;

const TrophyDiv = styled.div`
  position: relative;
  width: 150px;
  margin: 0 auto;
  @media (min-width: 900px) {
    width: 200px
    margin-right: 0;
    margin-left: 0;
  }
  @media (min-width: 1200px) {
    width: 250px;
  }
`;

const ScorecardDiv = styled.div`
  margin-top: 2em;
  margin-bottom: 2em;
`;

const CTADiv = styled.div`
  color: ${props => props.theme.colorGreen};
  border: 2px solid ${props => props.theme.colorGreen};
  padding: 1em;
  background: white;
  display: flex;
  flex-direction: column;

  a {
    text-decoration: none !important;
    color: inherit !important;
    display: flex;
  }

  p {
    text-align: center;
    font-weight: 600;
  }

  @media (min-width: 1200px) {
    align-items: center;
    padding: 1em 4em;
  }
`;

const ShareYourResultsDiv = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 1em;
  margin-bottom: 1em;
  flex-direction: column;
  p {
    text-transform: uppercase;
    font-weight: 700;
    color: ${props => props.theme.colorGreen};
  }
  div {
    &:focus {
      outline-width: 0px !important;
      outline: none !important;
    }
  }
  @media (min-width: 900px) {
    flex-direction: row;
    margin-top: 2em;
    margin-bottom: 0;
  }
`;

const ShareIconsDiv = styled.div`
  display: flex;
  justify-content: space-around;
  width: 150px;
`;

const ShareIconDiv = styled.div`
  height: 20px;
  width: 20px;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const ShareIconImg = styled.img`
  height: 100%;
  width: auto;
  transition: height 0.3s ease;
`;

const Img = styled.img`
  width: 100%;
`;

const PointsValue = styled.p`
  font-size: 24px;
  width: 100%;
  text-align: center;
  position: absolute;
  font-weight: 700;
  top: 4px;
  @media (min-width: 600px) {
    top: 4px;
    font-size: 24px;
  }
  @media (min-width: 900px) {
    top: 8px;
    font-size: 26px;
  }
  @media (min-width: 1200px) {
    top: 20px;
    font-size: 28px;
  }
`;
const PointsP = styled.p`
  font-size: 14px;
  top: 44px;
  font-weight: 600;
  width: 100%;
  text-align: center;
  position: absolute;
  @media (min-width: 600px) {
    top: 36px;
    font-size: 18px;
  }
  @media (min-width: 900px) {
    top: 48px;
    font-size: 22px;
  }
  @media (min-width: 1200px) {
    font-size: 28px;
    top: 50px;
  }
`;

const ButtonsDiv = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 4em;
`;
