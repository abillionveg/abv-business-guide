import businessNames from "./businessNames.js";
import externalLinks from "./externalLinks.js";

function getShareResultsText(totalPoints, quizLink, showQuizLink) {
  const result = `How vegan friendly is your business?  I scored ${totalPoints}/25.  Take the quiz now at ${
    showQuizLink === true ? quizLink : ""
  }${showQuizLink === true ? "!" : ""}`;
  return result;
}

const copy = {
  en: {
    tickerBreakingNews: "Breaking News",
    tickerVegans: "The rise of Veganism is showing no sign of slowing down.",
    overviewText:
      "Climate change, animal exploitation, and people making more conscious decisions for health — has led to the proliferation of Veganism. With more people switching to a plant-based lifestyle, it's time that businesses start to accommodate to this growing demographic. How retailers are responding to the growing appetite for plant-based food. Veganism has turned from a niche trend to a popular way of life, and many industries are adapting to meet the rising demand for meat and dairy free dishes. Consumers being both more aware of big animal agriculture, its impact on the environment and their own health.",
    overviewText2Para:
      "Climate change, animal exploitation, and people making more conscious decisions for health — has led to the proliferation of Veganism. With more people switching to a plant-based lifestyle, it's time that businesses start to accommodate to this growing demographic. How retailers are responding to the growing appetite for plant-based food. Veganism has turned from a niche trend to a popular way of life, and many industries are adapting to meet the rising demand for meat and dairy free dishes. Consumers being both more aware of big animal agriculture, its impact on the environment and their own health.",
    whatIsVeganFood: {
      title: "What is vegan food?",
      body:
        "Vegan food doesn’t contain any meat, seafood, dairy, eggs, or honey. Many times, we also interchange the word 'plant-based' with vegan, and while vegans and plant-based individuals normally consume the same diets, vegans will also avoid other forms of animal exploitation. This means that vegans don't wear leather, use cosmetics that aren't cruelty-free etc., and try to eliminate animal products from all aspects of their lives. "
    },
    whyShouldIVeganize: "Why should I veganize my menu?",
    aBoomingVeganMarket: {
      title: "A booming vegan market",
      body: [
        "Vegan and plant-based businesses have started making their mark in the food industry. According to the Plant-Based Foods Association (PBFA), sales of plant-based foods in the US went up 20% in 2018 (as opposed to 2% up for all food), showing that plant-based foods dollar sales are outpacing those of all retail foods by about 10x! This means that not only vegetarians or vegans but also mainstream consumers are starting to enjoy these options!",
        "As plant-based foods in the market increase, there are signs that this market will continue to grow. Startups like Impossible Foods, Beyond Meat, Omnipork, and Just Inc. are leading the way with their innovative plant-based alternatives that are almost just like the original."
      ]
    },
    morePeopleGoingVegan: {
      title: "More people are going vegan",
      body: [
        "More and more people, especially millennials, are going plant-based for various reasons  -animals, the planet, and personal health.",
        "In the US, there has been a 600% increase in people identifying as vegans from 2015-2018; In the U.K., the number of people identifying as vegans in 2018 increased by 350% compared to a decade ago.",
        "In Asia, the vegan diet is growing too: vegan claims on new food and drink products increased by around 400% in Southeast Asia from 2012 to 2016!",
        "More influencers, including professional athletes and A-list celebrities (Lewis Hamilton, Zac Efron, Beyonce, Gisele Bundchen, Natalie Portman, to name a few) have started transitioning to a more plant-based/vegan diet because of the environment, their health, and other factors."
      ]
    },
    buildCommunity: {
      title: "Build community and don't get left behind",
      body: [
        "By veganizing your menu, you'll show consumers that you're a conscious business that is trying to reduce their environmental impact, improve sustainability, and support animal welfare. This boosts your reputation and image as a business that is flexible, receptive, and open to change.",
        "Veganizing your menu shows your customers that you are inclusive. Doing this ensures that you don't exclude potential customers - and by offering plenty of tasty/good plant-based options, many of these vegan or environmentally-conscious customers will keep coming back!"
      ]
    },
    reduceEnvImpact: {
      title: "Reduce your environment impact",
      body: [
        "Globally, animal agriculture is responsible for more greenhouse gases than all the world's transportation system combined. Runoff of animal excrement from factory farms and livestock grazing is one of the most significant causes of pollution in our rivers and lakes.",
        "By providing more vegan options and aiming to be more plant-based, your business will be able to reduce your environment, conversely becoming more sustainable.",
        "Building a sustainable business doesn't only encompass food, though. Your business can further reduce your environmental impact by minimising disposable waste such as plastic cutlery and straws, giving diners an option to bring their own containers when taking out, and recycling glass and cardboard instead of throwing it away. These all contribute to our carbon footprint and our impact on our environment."
      ]
    },
    howVeganFriendlyAreYou: "How vegan friendly are you?",
    takeOurQuiz: "Take our quiz to find out",
    veganizeMenu: {
      title: "5 ways to Veganize your menu",
      labels: [
        "01. MOOve over, dairy",
        "02. I don't want no Beef",
        "03. Not just plain lettuce, please?",
        "04. Clear it up",
        "05. It's not exclusive..."
      ],
      body: [
        [
          "Adding dairy-free milk options is an easy way to make drinks suitable for vegans. Plant-based alternatives to milk include coconut milk, almond milk, soy milk, oat milk, and more! Have these on hand and make sure people know that these alternatives are available.",
          "There are currently plant-based butter, eggless mayo, vegan cheese, and other dairy alternatives that can be easily found in shops and supermarkets. Having these items on hand will allow you to easily make already vegetarian dishes vegan just by swapping a few ingredients. For example, you can add plant-based mayo instead of regular mayo to a veggie sandwich to make it vegan without much extra effort!"
        ],
        [
          "There are many vegan meat and meat substitutes that are readily available in the market! Many of the textures and flavours can be emulated to make things taste like (or even better than) actual meat.",
          "Some vegan meat substitutes include tofu, which has an amazing ability to absorb flavours through spices and marinades, tempeh, which is made of fermented soybeans and has a nuttier flavour, or seitan, processed wheat gluten that has the most meat-like texture.",
          "Other vegetables and fruits that have gained popularity in the vegan food world are jackfruits, which can be used to make pulled 'pork', or mushrooms, which have that savory, meaty umami taste.",
          "Many faux alternatives are trying to replicate the taste, texture, smell, and even 'bleeding' of beef using plant-based alternatives. These innovative faux meat products are taking the world by storm, and can be great additions to your menu for both plant-based and regular diners!"
        ],
        [
          "Be creative and innovative. A “salad” of some chopped lettuce and tomatoes or a “pizza” with just dough and tomato sauce won’t satisfy anyone. Take the challenge to explore colorful and flavorful fruits and vegetables and see how you can create something new! Many times, the best dishes are made using locally sourced seasonal fruits and veggies.",
          "Ideally, try to have at least 3-6 options (2 appetisers, mains, and desserts) so that vegans will have a variety of dishes to choose from when dining at your restaurant/cafe.",
          "Make the titles of your vegan dishes appetising and descriptive, too! Research shows that the use of “indulgent” and aspirational terms leads to more consumption of vegan and vegetarian meals. When Sainsbury called a “meat-free breakfast” a “feel good fry-up” resulted in a 7% increase in sales, and calling it a “field-grown breakfast” led to an 18% increase. Indulgent and aspirational terms allow these dishes to not only be limited to vegans but for all your customers!",
          "One of the key reasons that people may go vegan is because of the nutritional benefits that come with it. When making vegan dishes, try to maximize those health benefits by using fresh ingredients and creating dishes that are healthy, filling, and tasty! This will also attract not only vegans but also a bigger crowd of health-conscious individuals."
        ],
        [
          "One simple way to “veganize” your menu is by merely adding labels! It might not sound like a big step, but adding “V” or some sort of sign to label things as a vegan (or show that the item can be made veganized) communicates to the customer that you are willing to accommodate to their dietary choices.",
          "Train your staff and chefs so they are clear about which dishes are vegan and which dishes can be veganized. Clarity leads to efficient and effective communication — the key to a desirable experience for all!",
          "Making vegan options clear greatly enhances the experience for vegans eating out. By making their dining experience go a lot smoother, your customers leave with positive remarks, share their experience with their other vegan friends (such as through our app @abillionveg) and likely come back for more."
        ],
        [
          "Vegan food isn’t just for vegans — in fact, vegan options can be for anyone who wants to adopt a more conscious lifestyle: eat less meat, eat healthier, or just lessen their impact on the planet.",
          "There are many different ways vegan food can cater to a bigger crowd. One way is to create an entirely new “normal” menu for health-conscious and plant-based individuals (Read about how Pizza Express successfully launched its vegan ‘Verde’ menu here). By making vegan dishes and ingredients a normal part of the menu, rather than just a gimmick, your business can normalize plant-based food trends and draw in a broader consumer base.",
          "Another way to veganize your menu is to integrate plate-based options into a regular part of the menu, rather than putting it into a separate ‘vegetarian’ section, which can segregate these dishes. Even consider making a plant-based dish your “special” of the day — this allows more people to see that vegan food can easily fit into their lifestyle!"
        ]
      ]
    },
    howDifferentBusinessesVeganize: {
      title: "How different businesses veganized their menu",
      subtitle:
        "To give you some further inspiration, you can read about how different businesses veganized their menus or about how PizzaExpress successfully launched an entire vegan menu, Verde."
    },
    businessCaseStudies: [
      {
        company: businessNames.PIZZAEXPRESS,
        writeup: [
          "PizzaExpress released an entirely new veg menu called Verde. While their original menu had several vegetarian and vegan items already, this new menu offered a full range of dishes and had plant-based versions of their famous dough balls, a ton of pizzas and pastas made with Impossible Meats and Omnipork, and even a vegan apple tart!",
          "Pizza Express were able to veganize some of their original dishes such as the dough balls by replacing the dips of garlic butter and pesto with eggplant, sundried tomato, and walnut dips instead. For their pizzas and pastas, they went through multiple trials and tastings before deciding on using Impossible meats as a topping to their teriyaki and curry pizza, as well as using Omnipork for their Mushroom and Fennel pizza. These pizzas also had vegan mozzarella instead of normal cheese!",
          "Read more how Pizza Express successfully created their first vegan menu in our case study."
        ]
      },
      {
        company: businessNames.CHIPOTLE,
        writeup: [
          "In terms of adding vegan and vegetarian options to their menus, Chipotle was early to the game. In 2014, Chipotle added Sofritas, a meatless protein made of organic tofu that’s shredded and braised with chipotle chillis, roasted poblanos, and spices.",
          "They hoped that adding this new product would not only allow for vegans and vegetarians with an additional option rather than just an extra-large dollop of guacamole, but also appeal to an ever larger consumer base.",
          "In 2019, Chipotle also released a vegan and a vegetarian bowl in their new “Lifestyle Bowls” line as a way to make people aware of their menu options such as their plant-based sofrita protein. Even those who don't want to add the sofrita protein can opt for the many vegan choices that chipotle offers: rice, guacamole, a variety of beans, 4 different options of salsa, romaine lettuce."
        ]
      },
      {
        company: businessNames.PRET,
        writeup: [
          "Pret a Manger, a high street coffee shop with locations in Europe, Asia, and the USA, is now gearing towards vegan and vegetarian food. In April of 2019, they released 20 new products, with 8 being vegan, 6 vegetarian, and 6 containing meat. Some of their new options included coconut and berries on gluten free bread for breakfast, Asian-style veggie box and hummus and chipotle wrap for lunch.",
          "After receiving an “overwhelming” response to their meat-free food, the restaurant actually started opening Veggie Prets, which are chains of Pret a Manger that only sell vegetarian food. In addition to that, they continue to add vegetarian and vegan options to their menu to cater to their growing plant-based customer base.",
          "Pret a Manger have also started championing sustainability on the single-use front too — in 2018, they announced a plan to eliminate single-use plastic from all its stores to improve its impact on the planet."
        ]
      }
      // {
      //   company: businessNames.EMPRESS,
      //   writeup: [
      //     "Empress is an elegant fine dining Chinese Restaurant offering Cantonese-style dishes in a contemporary setting in Singapore. While their original menu is not fully vegan or vegetarian, Empress has a fully vegetarian set menu called “Empress Vegetarian Signatures Sharing Menu” which still include lots of traditional Chinese dishes with a spin like their Sichuan Impossible meat and Mapo tofu in Hotpot, Kung pao wok-charred cauliflower, or their sweet & sour vegetarian ‘Char Siew’. While this menu is labelled “vegetarian”, 18/22 dishes are actually vegan.",
      //     "Empress also offers many other vegan dishes using a variety of ingredients such as Omnipork, a plant-based pork replacement, created by Hong Kong startup Right treat. Using this alternative, Empress created dishes like their “Sweet and Sour Omnipork Balls”. They also offer several vegan desserts such as their “Chairman’s Bubur Char-char”, a spin on the Singaporean classic, or “Chocolate and Peanut”, a vegan chocolate mousse paired with peanut coconut ice cream and other interesting toppings.",
      //     "By offering a vegetarian set menu and numerous other vegan ala-carte dishes to their menu, Empress offers the same fine dining, Chinese meets western experience to their plant-based customers, accomadating a wider customer base and building a strong reputation."
      //   ]
      // },
      // {
      //   company: businessNames.CAFES,
      //   writeup: [
      //     "Numerous Cafes around the world have started to offer alternatives to dairy milk; however, many of these cafes add additional charges for these plant-based alternatives. By not charging more for plant-based milk alternatives, cafes like Stumpton Coffee Roasters are showing their support for the vegan community.",
      //     "There are many different plant-based milk options, however the most common milk alternatives are soy milk, almond milk, and oat milk. Having at least one plant-based milk option on hand is a great place to start. Being vegan isn't premium anymore; it's the norm.",
      //     "SOY MILK: considered the “OG” standard dairy-free milk, soy milk is readily available, affordable, and can be easily substituted. That being said, its flavor and texture varies, and it also tends to curdle when heated; as a result, there should be experimentation done with the brand of soy milk and the speed and temperature when steaming soy milk.",
      //     "ALMOND MILK: Reportedly one of the most popular plant milks worldwide, almond milk has a slightly sweet and nutty taste. It also has a tendency to curdle like soy, but is slightly easier to steam.",
      //     "OAT MILK: Over the recent years, this type of milk has gained lots of popularity, its creamy texture and balanced flavour making it a great substitute for dairy. Some say that it also steams and froths well compared to other milks, making it easier to use as a direct substitute."
      //   ]
      // }
    ],
    readMore: "Read more",
    veganBusinessCaseStudies: "Vegan Business Case Studies",
    getStartedYourself: "Get Started Yourself",
    getStartedYourselfSubheading:
      "Now that we’ve given you the whys and hows, hopefully you’re inspired to start taking action. Since taking the first step is usually the hardest part, here’s a list to help you!",
    getStartedYourselfContent: [
      {
        title: "Food Dictionary",
        blurb:
          "In our Vegan Dictionary, you'll be introduced to popular vegan food items as well as common vegan substitutes to meat and dairy products.",
        link: externalLinks.getYourselfStarted1
      },
      {
        title: "Cheat Sheet",
        blurb:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud nisi ut Excepteur sint laborum.",
        link: externalLinks.getYourselfStarted2
      },
      {
        title: "Lorem Ipsum",
        blurb:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud nisi ut Excepteur sint laborum.",
        link: externalLinks.getYourselfStarted3
      }
    ],
    view: "View",
    conclusion: "Conclusion",
    conclusionPara:
      "With people becoming more conscious about their health, the environment, and animals, there's no doubt that the vegan demographic will only continue to grow. It's your turn to do your part, and that starts by veganizing your menu. So be inspired, be creative, and make the world a better place. Don't get left behind. Be a game-changer.",
    claimYourBusiness: "Claim your business",
    shareThisGuide: "Share this guide",
    startQuiz: "Start Quiz",
    howVeganFriendly: "How Vegan-friendly are you?",
    selectAllThatApply: "Select all that apply",
    noneOfTheAbove: "None of the above",
    quizSections: [
      {
        title: "Sustainability",
        blurb:
          "Sustainability for restaurants means operating in a way that protects, preserves or restores the natural environment, promotes social equity, enhances the lives of people and communities.",
        options: [
          {
            name: "I don't offer plastic straws."
          },
          {
            name: "I don't use plastic single-use takeaway boxes."
          },
          {
            name: "I have made efforts to reduce both food and other waste."
          },
          {
            name: "I use local ingredients for my dishes when possible."
          },
          {
            name: "I actively encourage my customers to live more sustainably."
          },
          {
            name: "None of the above"
          }
        ]
      },
      {
        title: "Innovation",
        blurb:
          "Going vegan friendly takes some seriously innovative thinking. It takes a public-facing role instituting ethical farm-to-table sourcing practices, finding innovative ways to offer vegetarian, vegan, and plant-based menu items. The goal is to make them more attractive and easy to order for vegans, but especially so for meat eaters. How is your business leveraging whole foods that are naturally high in protein, creating crave-worthy vegan options and redesigning menus to make veggie-packed dishes easier to order?",
        options: [
          {
            name: "I have introduced a new vegan option in the last 3 months."
          },
          {
            name: "I serve more than one type of vegan food."
          },
          {
            name: "I have improved a vegan option based on customer's feedback."
          },
          {
            name: "I use seasonal fruits and vegetables to create ingredients."
          },
          {
            name:
              "I put in the effort to make my vegan dishes nutritious, tasty, and filling."
          },
          {
            name: "None of the above"
          }
        ]
      },
      {
        title: "Options",
        blurb:
          "Increasingly, we live in a health and environmentally conscious society. Many people are more selective about what they want to -or what they can- eat. Consumer preferences seem to indicate that vegan or vegetarian options are not unique anymore. If you want to appeal to millenials, they are essential. ",

        options: [
          {
            name: "I offer dairy-free milk options."
          },
          {
            name:
              "Vegan cheese, mayonnaise, yogurt, and ice cream are alternatives I have on hand."
          },
          {
            name:
              "I have experimented with vegan different vegan protein substitutes."
          },
          {
            name: "At least 3 items on my menu can be 'veganized'."
          },
          {
            name:
              "I have at least 2 appetisers, main dishes, and desserts that are vegan."
          },
          {
            name: "None of the above"
          }
        ]
      },
      {
        title: "Labeling",
        blurb:
          "The increase of regulatory oversight of restaurants have increased. Therefore the need for businesses to create careful plans to address food allergens, from ensuring that food products suppliers provide comprehensive allergen checklists to training food handlers in methods to prevent issues like miscommunication between and among restaurant staff and customers, unexpected or hidden food allergens, and cross-contact during food preparation. ",
        options: [
          {
            name:
              "I have labelled vegan and vegetarian options clearly, either through symbols or words."
          },
          {
            name:
              "I have labelled the most common allergens — tree nuts, peanuts, eggs, milk, wheat, soy."
          },
          {
            name:
              "My chefs know which dishes are vegan, and which dishes can be veganized."
          },

          {
            name:
              "Names of my vegan dishes are descriptive and appetising, but still accurate."
          },
          {
            name:
              "My waiting staff know which dishes are vegan, and which dishes can be veganized."
          },
          {
            name: "None of the above"
          }
        ]
      },
      {
        title: "Inclusivity",
        blurb:
          "There's now a growing demand for plant-based foods that goes beyond vegans. This demand means there's a massive opportunity for you to bring more customers through your doors and capture a growing share of the market. How will your business take advantage of this opportunity?",
        options: [
          {
            name: "I have made a vegan dish my 'special' of the day."
          },
          {
            name: "Vegan dishes are integrated into my menu."
          },
          {
            name:
              "Vegans are aware that they have options when dining at my establishment."
          },
          {
            name:
              "Non-vegans regularly order vegan options because it's delicious."
          },
          {
            name:
              "Vegans have the flexibility to 'veganize' dishes at my establishment."
          },
          {
            name: "None of the above"
          }
        ]
      }
    ],
    results: "results",
    computedResults: [
      {
        pointsLowerBound: 0,
        pointsUpperBound: 4,
        title: "Time to get started...",
        message:
          "Ok, so your business isn't too vegan-friendly right now. But don't fret — we've got many resources that can get you started!",
        cta:
          "Read about how to veganize your menu and make your business more sustainable and vegan-friendly!",
        button: "GET STARTED"
      },
      {
        pointsLowerBound: 5,
        pointsUpperBound: 9,
        title: "OK, that's a start!",
        message:
          "Hey, at least you've got started! We have a few resources to help you on your journey.",
        cta: " Learn how to claim your business on abillionveg!",
        button: "TAKE THE NEXT STEP"
      },
      {
        pointsLowerBound: 10,
        pointsUpperBound: 14,
        title: "You're on the right track!",
        message:
          "Not bad, not bad! You're cruising in the right direction, so keep pushing on!",
        cta: "Check out our guide for 5 easy ways to veganize your menu.",
        button: "#VEGANIZE IT"
      },
      {
        pointsLowerBound: 15,
        pointsUpperBound: 20,
        title: "Almost there, keep the ball rolling!",
        message:
          "Ooh, you're on a roll! The midpoint hill has been conquered; keep checking those boxes off to become a better business.",
        cta:
          "Check out our business case studies on companies that are making sustainability tasty and appealing to the masses.",
        button: "LEARN MORE"
      },
      {
        pointsLowerBound: 21,
        pointsUpperBound: 25,
        title: "You're rocking it!",
        message:
          "Wow, you've got this down! Now it's your turn to spread this message on to other businesses and teach them how to veganize their menu themselves!",
        cta:
          "Share our business transition guide to help other businesses veganize their menu.",
        button: "GET INVOLVED"
      }
    ],

    points: "points",
    point: "point",
    shareYourResults: "Share your results",
    shareOurBizGuide:
      "Share our business transition guide to help other businesses veganize their menu.",
    next: "Next",
    back: "Back",
    shareResultsText: function(totalPoints, quizLink, showQuizLink) {
      return getShareResultsText(totalPoints, quizLink, showQuizLink);
    }
  }
};

export default copy;
