const externalLinks = {
  getYourselfStarted1: "#",
  getYourselfStarted2: "#",
  getYourselfStarted3: "#",
  claimYourBusiness: "https://business.abillionveg.com",
  businessGuideShareLink: "https://abillionveg-business-guide.herokuapp.com"
};

export default externalLinks;
