import React from "react";
import styled from "styled-components";

const Button = ({
  text,
  bgColor,
  isUppercase,
  moreStyles,
  icon,
  iconPosition
}) => {
  return (
    <Container
      moreStyles={moreStyles}
      bgColor={bgColor}
      isUppercase={isUppercase}
    >
      {!!icon &&
        iconPosition === "left" && (
          <ImgDiv iconPosition={iconPosition}>
            <Img src={icon} />
          </ImgDiv>
        )}

      {text}
      {!!icon &&
        iconPosition === "right" && (
          <ImgDiv iconPosition={iconPosition}>
            <Img src={icon} />
          </ImgDiv>
        )}
    </Container>
  );
};

export default Button;

const Container = styled.button`
  background: ${props => props.bgColor};

  border-style: none !important;
  text-transform: ${props => (props.isUppercase ? "uppercase" : "")}
  color: white;
  border-radius: 50px;
  height: 40px;
  color: white;
  display: flex;
  align-items: center;
  font-size: 14px;
  margin: 1em auto;
  font-weight: 500;
  padding: 0 2.5em;
  white-space: nowrap;
  cursor: pointer;
  &:hover {
    opacity: 0.8;
    transition: opacity 0.5s ease;
  }
  &:focus {
    outline: none !important;
    outline: 0;
    outline-width: 0px !important;
  }

  &:active {
    border-style: none !important;
  }

  @media(min-width: 600px){
    font-size: 16px
  }
  ${props => props.moreStyles}
`;

const ImgDiv = styled.div`
  width: 20px;
  height: 20px;
  padding-left: ${props => props.iconPosition === "right" && "10px"};
  padding-right: ${props => props.iconPosition === "left" && "10px"};
`;

const Img = styled.img`
  width: 100%;
`;
