import React from "react";
import styled from "styled-components";

const Breadcrumbs = ({ breadcrumbItems }) => {
  return (
    <Container>
      {breadcrumbItems.map((item, index) => (
        <Span key={index} isLastItem={index === breadcrumbItems.length - 1}>
          {item}
          {index < breadcrumbItems.length - 1 ? " / " : ""}
        </Span>
      ))}
    </Container>
  );
};

export default Breadcrumbs;

const Container = styled.div`
  padding-right: 30px;
  @media (min-width: 600px) {
    padding-right: 0;
    display: flex;
    align-items: center;
  }
`;
const Span = styled.span`
  font-size: 12px;
  font-weight: ${props => props.isLastItem && "600"};
`;
